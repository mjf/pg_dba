select
  (
    select
      system_identifier
    from
      pg_control_system()
  )::text cluster_id,
  case
  when pg_is_in_recovery() then
    'standby'
  else
    'primary'
  end::text node_role,
  case
  when
    pg_last_wal_receive_lsn() = pg_last_wal_replay_lsn()
  then
    0::decimal
  else
    extract(epoch from now() - pg_last_xact_replay_timestamp())::decimal
  end log_delay;
