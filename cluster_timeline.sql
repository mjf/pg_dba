select
  (
    select
      system_identifier
    from
      pg_control_system()
  )::text cluster_id,
  case
  when pg_is_in_recovery() then
    'standby'
  else
    'primary'
  end::text node_role,
  (
    'x' || lpad(substring(pg_walfile_name(pg_current_wal_lsn()), 1, 8), 8, '0')
  )::bit(32)::integer timeline
where
  (
    select
      not pg_is_in_recovery()
  );
