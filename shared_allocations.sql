with shmem_allocs as (
  select
    name,
    size,
    allocated_size
  from
    pg_shmem_allocations 
)
select (
  select
    system_identifier
  from
    pg_control_system()
  )::text cluster_id,
  case
  when pg_is_in_recovery() then
    'standby'
  else
    'primary'
  end::text node_role,
  lower(coalesce(name, 'free'))::text allocation_name,
  size::bigint size_bytes,
  allocated_size::bigint allocated_size_bytes
from
  shmem_allocs;
