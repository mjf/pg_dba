select
  (
    select
      system_identifier
    from
      pg_control_system()
  )::text cluster_id,
  case
    when
      pg_is_in_recovery()
    then
      'standby'
    else
      'primary'
  end::text node_role,
  coalesce(
    database.datname,
    '<shared object>'
  )::text database_name,
  database.numbackends::bigint backends_instant_count,
  database.xact_commit::bigint transactions_committed_total_count,
  database.xact_rollback::bigint transactions_rollbacked_total_count,
  database.blks_read::bigint blocks_read_total_count,
  database.blks_hit::bigint blocks_hit_total_count,
  database.tup_returned::bigint tuples_returned_total_count,
  database.tup_fetched::bigint tuples_fetched_total_count,
  database.tup_inserted::bigint tuples_inserted_total_count,
  database.tup_updated::bigint tuples_updated_total_count,
  database.tup_deleted::bigint tuples_deleted_total_count,
  database.conflicts::bigint conflicts_total_count,
  database.temp_files::bigint temporary_files_total_count,
  database.temp_bytes::bigint temporary_bytes_total_count,
  database.deadlocks::bigint deadlocks_total_count,
  database.checksum_failures::bigint checksum_failures_total_count,
  coalesce(
    extract(
      epoch
    from
      database.checksum_last_failure
    ),
  0)::bigint checksum_failures_last_epoch,
  cast(
    database.blk_read_time / 1000.0
  as
    double precision
  ) blocks_read_time_total_seconds,
  cast(
    database.blk_write_time / 1000.0
  as
    double precision
  ) blocks_write_time_total_seconds,
  cast(
    database.session_time / 1000.0
  as
    double precision
  ) sessions_time_total_seconds,
  cast(
    database.active_time / 1000.0
  as
    double precision
  ) states_active_time_total_seconds,
  cast(
    database.idle_in_transaction_time / 1000.0
  as
    double precision
  ) states_idle_in_transaction_time_total_seconds,
  database.sessions::bigint sessions_total_count,
  database.sessions_abandoned::bigint sessions_abandoned_total_count,
  database.sessions_fatal::bigint sessions_fatal_total_count,
  database.sessions_killed::bigint sessions_killed_total_count,
  coalesce(
    database_conflicts.confl_tablespace,
    0
  )::bigint conflicts_tablespace_dropped_total_count,
  coalesce(
    database_conflicts.confl_lock,
    0
  )::bigint conflicts_lock_timeouted_total_count,
  coalesce(
    database_conflicts.confl_snapshot,
    0
  )::bigint conflicts_snaphost_expired_total_count,
  coalesce(
    database_conflicts.confl_bufferpin,
    0
  )::bigint conflicts_buffer_pinned_total_count,
  coalesce(
    database_conflicts.confl_deadlock,
    0
  )::bigint conflicts_deadlocked_total_count,
  coalesce(
    age(database_information.datfrozenxid),
    0
  )::bigint xid_frozen_age,
  coalesce(
    age(database_information.datfrozenxid)::double precision / 2e9,
    0
  )::double precision xid_consumed_ratio,
  coalesce(
    2^31 - 1 - age(database_information.datfrozenxid),
    0
  )::bigint xid_to_wraparound_count,
  coalesce(
    current_setting('autovacuum_freeze_max_age')::bigint - age(database_information.datfrozenxid),
    0
  )::bigint xid_to_aggressive_autovacuum_count,
  coalesce(
    mxid_age(database_information.datminmxid),
    0
  )::bigint mxid_age,
  coalesce(
    mxid_age(database_information.datminmxid)::double precision / (2^31 - 1),
    0
  )::double precision mxid_consumed_ratio,
  coalesce(
    2^31 - 1 - mxid_age(database_information.datminmxid),
    0
  )::bigint mxid_to_wraparound_count,
  coalesce(
    2^31 - 1 - current_setting('autovacuum_multixact_freeze_max_age')::bigint - mxid_age(database_information.datminmxid),
    0
  )::bigint mxid_to_aggressive_autovacuum_count
from
  pg_stat_database database
  left outer join
    pg_stat_database_conflicts database_conflicts
  on
    database_conflicts.datid = database.datid
    left outer join
      pg_database database_information
    on
      database_information.oid = database.datid;
