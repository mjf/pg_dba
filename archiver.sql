select
  (
    select
      system_identifier
    from
      pg_control_system()
  )::text cluster_id,
  case when
    pg_is_in_recovery()
  then
    'standby'
  else
    'primary'
  end::text node_role,
  archived_count::bigint,
  extract(
    epoch from now() - last_archived_time
  )::integer last_archived_age_seconds,
  failed_count::bigint failed_archivals_count,
  case when
    last_failed_wal is null or
    last_failed_wal <= last_archived_wal
  then
    0
  else
    extract(
      epoch from now() - last_failed_time
    )
  end::integer last_failed_archive_age_seconds,
  (
    current_setting('archive_mode')::boolean and
    (
      last_failed_wal is null or
      last_failed_wal <= last_archived_wal
    )
  )::integer is_archiving,
  pg_wal_lsn_diff(
    pg_current_wal_lsn(), '0/00000000'::pg_lsn
  )::bigint archived_volume_bytes,
  (
    select
      count(*)
    from
      pg_ls_archive_statusdir()
    where
      name ~ '.ready'
  )::bigint lag_files_count
from
  pg_stat_archiver
where
  (
    select
      not pg_is_in_recovery()
  );
