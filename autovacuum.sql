with
  autovacuum_activity
  (
    database_name,
    duration_seconds,
    query
  )
  as (
    select
      d.datname database_name,
      extract(
        epoch
      from
        current_timestamp - a.query_start
      ) duration_seconds,
      a.query query
    from
      pg_stat_activity a
      left outer join
        pg_database d
      on
        a.datid is not distinct from d.oid
    where
      a.query like 'autovacuum: %%'
    group by
      database_name,
      duration_seconds,
      query
  )
select (
  select
    system_identifier
  from
    pg_control_system()
  )::text cluster_id,
  case
  when pg_is_in_recovery() then
    'standby'
  else
    'primary'
  end::text node_role,
  coalesce(a.database_name, '<none>')::text database_name,
  count(a.*) filter(
    where
      a.query like 'autovacuum: %(to prevent wraparound)%'
  )::bigint autovacuum_to_prevent_wraparound_count,
  count(a.*) filter(
    where
      a.query like 'autovacuum: VACUUM%%'
  )::bigint autovacuum_vacuum_count,
  count(a.*) filter(
    where
      a.query like 'autovacuum: VACUUM ANALYZE%%'
  )::bigint autovacuum_vacuum_analyze_count,
  count(a.*) filter(
    where
      a.query like 'autovacuum: ANALYZE%%'
  )::bigint autovacuum_analyze_count,
  count(a.*) filter(
    where
      a.query like 'autovacuum: BRIN summarize%%'
  )::bigint autovacuum_brin_summarize_count,
  count(a.*)::bigint autovacuum_total_count,
  round(min(a.duration_seconds), 3)::double precision autovacuum_duration_min_seconds,
  round(avg(a.duration_seconds), 3)::double precision autovacuum_duration_avg_seconds,
  round(max(a.duration_seconds), 3)::double precision autovacuum_duration_max_seconds,
  round(stddev(a.duration_seconds), 3)::double precision autovacuum_duration_stddev_seconds
from
  autovacuum_activity a
group by
  database_name;
