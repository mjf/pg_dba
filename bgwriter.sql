select (
  select
    system_identifier
  from
    pg_control_system()
  )::text cluster_id,
  case
  when pg_is_in_recovery() then
    'standby'
  else
    'primary'
  end::text node_role,
  bgw.checkpoints_timed::bigint checkpoint_timed_count,
  bgw.checkpoints_req::bigint checkpoint_requested_count,
  bgw.checkpoint_write_time::bigint checkpoint_write_time,
  bgw.checkpoint_sync_time::bigint checkpoint_sync_time,
  bgw.buffers_checkpoint::bigint buffer_checkpoint_count,
  bgw.buffers_clean::bigint buffer_clean_count,
  bgw.buffers_alloc::bigint buffer_allocation_count,
  bgw.buffers_backend::bigint backend_buffer_count,
  bgw.buffers_backend_fsync::bigint backend_buffer_fsync_count,
  bgw.maxwritten_clean::bigint written_clean_max
from
  pg_stat_bgwriter bgw;
