select
  (
    select
      system_identifier
    from
      pg_control_system()
  )::text cluster_id,
  case
  when pg_is_in_recovery() then
    'standby'
  else
    'primary'
  end::text node_role,
  coalesce(slot_name, '<none>')::text slot_name,
  pg_wal_lsn_diff(pg_current_wal_insert_lsn(), restart_lsn)::bigint wal_restart_lag,
  pg_wal_lsn_diff(pg_current_wal_insert_lsn(), confirmed_flush_lsn)::bigint wal_confirmed_flush_lag
from
  pg_replication_slots;
