with
  sys(id) as (
    select
      system_identifier
    from
      pg_control_system()
  )
select
  s.id::text cluster_id,
  coalesce(d.datname, '<shared object>')::text database_name,
  sum(b.usagecount)::bigint usage_count,
  count(*)::bigint total_count,
  count(distinct b.relfilenode)::bigint relations_count,
  count(distinct b.pinning_backends)::bigint pinning_backends_count,
  count(b.*) filter(where b.isdirty is true)::bigint dirty_count,
  count(
    (
      b.bufferid,
      b.relfilenode,
      b.reltablespace,
      b.reldatabase,
      b.relforknumber,
      b.relblocknumber,
      b.isdirty,
      b.usagecount,
      b.pinning_backends
    )
    is not distinct from (
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null
    )
  )::bigint unused_count
from
  sys s,
  pg_buffercache b
  left join
    pg_database d
  on
    d.oid = b.reldatabase
group by
  s.id,
  d.datname;
