select
  (
    select
      system_identifier
    from
      pg_control_system()
  )::text cluster_id,
  case
  when pg_is_in_recovery() then
    'standby'
  else
    'primary'
  end::text node_role,
  extract(
    epoch from
      date_trunc('seconds', now() - pg_postmaster_start_time())
  )::integer uptime,
  extract(
    epoch from
      date_trunc('seconds', now() - pg_conf_load_time())
  )::integer configuration_uptime,
  count(*) filter(
    where
      pending_restart is true
  )::integer pending_settings_count
from
  pg_settings;
