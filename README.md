# Postgres DBA toolset

This toolset provides analytical SQL queries for Postgres DBA. All of
the SQL queries provided **CAN** be used by both humans and collectors
of metrics.

## Humans

Just use in `psql(1)` as follows (or copy/paste):

```
\i /path/to/file.sql
```

## Collectors

### Telegraf

```toml
[[inputs.postgresql_extensible]]

  address = "user=collector password=*** database=postgres"
```

All *non-numeric* values **MUST** become tags (labels) including former
*boolean* values, if there is any returned by the SQL query.

```toml
[[inputs.postgresql_extensible.query]]

  measurement = "postgres_shared_buffers"
  script = "/path/to/shared_buffers.sql"
  tagvalue = "cluster_id,database_name"
```

You would get the following metrics for Postgres *shared buffers*:

- `cluster_id` (always returned by *all* scripts; **tag**)
- `database_name` (returned by *all* database-related scripts; **tag**)
- `usage_count` (**value**)
- `total_count` (**value**)
- `relations_count` (**value**)
- `pinning_backends_count` (**value**)
- `dirty_count` (**value**)
- `unused_count` (**value**)
