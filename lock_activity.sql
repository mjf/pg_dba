select (
  select
    system_identifier
  from
    pg_control_system()
  )::text cluster_id,
  case
  when pg_is_in_recovery() then
    'standby'
  else
    'primary'
  end::text node_role,
  case when database.oid = 0 then
    '<shared object>'
  else
    coalesce(database.datname, '<transaction>')
  end::text database_name,
  coalesce(backend.state, '<none>')::text backend_state,
  case when backend.backend_type in(
    'autovacuum launcher',
    'autovacuum worker',
    'logical replication launcher',
    'logical replication worker',
    'parallel worker',
    'background writer',
    'client backend',
    'checkpointer',
    'archiver',
    'startup',
    'walreceiver',
    'walsender',
    'walwriter'
  ) then
    backend.backend_type
  else
    lower(backend.backend_type)
  end:: text backend_type,
  coalesce(backend.wait_event_type, '<none>')::text backend_wait_event_type,
  coalesce(backend.wait_event, '<none>')::text backend_wait_event,
  sum(
    cardinality(
      pg_blocking_pids(backend.pid)
    )
  )::bigint blocking_backend_count,
  count(backend.*) filter(
    where
      backend.state is not distinct from 'active'
  )::bigint active_backend_count,
  count(backend.*) filter(
    where
      backend.state is not distinct from 'idle'
  )::bigint idle_backend_count,
  count(backend.*) filter(
    where
      backend.state is not distinct from 'idle in transaction'
  )::bigint idle_in_transaction_backend_count,
  count(backend.*) filter(
    where
      backend.state is not distinct from 'idle in transaction (aborted)'
  )::bigint idle_in_transaction_aborted_backend_count,
  count(backend.*) filter(
    where
      backend.state is not distinct from 'fastpath function call'
  )::bigint fast_path_function_call_backend_count,
  count(backend.*) filter(
    where
      backend.state is not distinct from 'disabled'
  )::bigint disabled_backend_count,
  coalesce(lock.locktype, '<none>')::text lock_type,
  coalesce(lock.mode, '<none>')::text lock_mode,
  count(lock.*) filter(
    where
      lock.fastpath
  )::bigint fast_path_lock_count,
  count(lock.*) filter(
    where
      not lock.granted
  )::bigint awaited_lock_count,
  count(lock.*) filter(
    where
      lock.relation is not null
  )::bigint relation_lock_count,
  count(lock.*) filter(
    where
      lock.virtualxid is not null
  )::bigint virtual_transaction_lock_count,
  count(lock.*) filter(
    where
      lock.transactionid is not null
  )::bigint transaction_lock_count,
  count(lock.*) filter(
    where
      lock.pid is null
  )::bigint prepared_transaction_lock_count,
  count(backend.*)::bigint total_backend_count,
  count(lock.*)::bigint total_lock_count
from
  pg_locks lock
  left outer join
    pg_database database
  on
    lock.database is not distinct from database.oid
    left outer join
      pg_stat_activity backend
    on
      lock.pid is not distinct from backend.pid
group by
  database_name,
  backend_state,
  backend_type,
  backend_wait_event_type,
  backend_wait_event,
  lock_type,
  lock_mode;
