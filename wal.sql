select (
  select
    system_identifier
  from
    pg_control_system()
  )::text cluster_id,
  case
  when pg_is_in_recovery() then
    'standby'
  else
    'primary'
  end::text node_role,
  count(*) filter(
    where
      wal.mtime >= now() - '1 minute'::interval
  )::bigint wal_count_1m,
  coalesce(
    sum(wal.sz) filter(
      where
        wal.mtime >= now() - '1 minute'::interval
    ), 0
  )::bigint wal_size_bytes_1m,
  count(*) filter(
    where
      wal.mtime >= now() - '5 minutes'::interval
  )::bigint wal_count_5m,
  coalesce(
    sum(wal.sz) filter(
      where
        wal.mtime >= now() - '5 minutes'::interval
    ), 0
  )::bigint wal_size_bytes_5m,
  count(*) filter(
    where
      wal.mtime >= now() - '15 minutes'::interval
  )::bigint wal_count_15m,
  coalesce(
    sum(wal.sz) filter(
      where
        wal.mtime >= now() - '15 minutes'::interval
    ), 0
  )::bigint wal_size_bytes_15m,
  count(*) wal_count_total,
  coalesce(
    sum(wal.sz),
    0
  )::bigint wal_size_bytes_total
from (
  select
    modification,
    size
  from
    pg_ls_waldir()
  ) wal(mtime, sz);
